// select a database
use <database name>

// when creating a new database via the command line, the "use" command can be entered with a name of a database that does not yet exist. Once a new record is inserted into that database, the database will be created.

//database = filing  cabinet
//collection = drawer
// document or record = folder inside a drawer
// sub-documents (optional) = other files
// fields = file content

/*
e.g. document with no sub-documents:
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor"
	}
e.g. document with sub-documents:
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor",
		address: {
			street: "123 Street St",
			city: "Makati",
			country: "Philippines"
		}
	}
*/

/*
Embedded vs Referenced data:

Referenced data:
Users:
	{
		id: 298 
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor"
	},
	{
		id: 301 
		name: "Jenie",
		age: 28,
		occupation: "Student"
	}

Orders: 
	{
		products:[
		{
			name: "new pillow",
			price: 300
		}
		],
		userID: 298
	}

Embedded data:

Users:
	{
		id: 298 
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor",
		orders: [
			{
				products:[
					{
						name: "new pillow",
						price: 300
					}
				]
			}
		]
	}

*/

// Insert One Document (Create)
db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
})
// if inserting or creating a new document within a  collection that does not yet exists, MongoDB will automatically create that collection.

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "123456789",
			email: "stephenhawking@mail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "123456789",
			email: "neilarmstrong@mail.com"
		},
		courses: ["React", "Lavarel", "SASS"],
		department: "none"
	}
])

// Finding Documents

// it will retrieve list of ALL users
db.users.find()

// find a specific user
db.users.find({firstName: "Stephen"}) //find any and all matches
db.users.findOne({firstName: "Stephen"}) //finds only the first match

// find documents with multiple parameters / condition
db.users.find({lastName: "Armstrong", age: 82})

//Update or Edit Documents

// Create a new document to update
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "123456789",
		email: "test@mail.com"
	},
	courses: [],
	department: "none"
}) 
// updateOne always needs to be provided two objects. FIRST object specifies which document to update. The second object contains the $set operator and inside the $set oprator are the spec ific fields to be updated.


db.users.updateOne(
	// what record you want to update
	{_id: ObjectId("62876c4ee7f355184ef2e518")},
	{
		// What will you update
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "123456789",
				email: "billgates@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	}
)

// update multiple documents
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
)
// creating a document to delete
db.users.insert({
	firstName: "test"
})

// deleting a single document
db.users.deleteOne({
	firstName: "test"
})

// deleting many records
// Be careful when using deleteMany. If no search criteria are provided, it will delete ALL documents within the given collection.
db.posts.deleteMany()

// create another user to delte
db.users.insert({
	firstName: "Bill"
})

db.users.deleteMany({
	firstName: "Bill"
})

